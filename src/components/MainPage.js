import {useReducer, useState} from "react";
import {Button, ButtonGroup, Container} from "@mui/material";
import FormikUI from "./FormikUI";
import {SidePane} from "react-side-pane";
import '../css/App.css'
import ContactPageIcon from '@mui/icons-material/ContactPage';

const MainPage = () => {

    const [open, dispatchOpen] = useReducer((prev) => !prev, false);


    const addForm = (values) => {
        alert('Wysłano wiadomość! ' + "Treść: " + values.msg)
        dispatchOpen()
    }

    return (
        <div style={{marginTop:'100px'}}>
                <div>
                    <h1>Strona poświęcona zespołowi Gorillaz</h1>
                </div>
                <img src={"https://media.resources.festicket.com/www/artists/Gorillaz_New.jpg"} alt={"band"}
                     style={{width: '30%', margin: '0 auto', marginBottom: '20px'}}/>
                <Container style={{paddingTop: "20px"}}>
                    <h3>Skontaktuj się z nami!</h3>
                    <ButtonGroup>
                        <Button onClick={() => dispatchOpen()} size="large" variant="contained"
                                style={{backgroundColor: "violet", marginBottom: '100px'}}
                                startIcon={<ContactPageIcon/>}>
                            Kontakt
                        </Button>
                    </ButtonGroup>
                </Container>
            <SidePane open={open} width={50} onClose={dispatchOpen}>
                <FormikUI addForm={addForm}/>
            </SidePane>
        </div>

    );

}

export default MainPage;