import {Container} from "@mui/material";
import YoutubeFilm from "./YoutubeFilm";
import { MDBContainer } from 'mdbreact';

const Info = () => {

    return (
        <div style={{marginTop:'100px'}}>
            <img src={"https://jazzsoul.pl/images//2020/10/gorillaz.jpg"} alt={"band"}
                 style={{width: '40%', margin: '0 auto'}} />
            <Container maxWidth={"sm"}>
                <MDBContainer>
                    <p className="text-monospace">Jesteśmy wirtualnym zespółem założonym przez Damona Albarna i Jamie Hewletta w 1998. Tworzymy muzykę
                        będącą połączeniem hip-hopu, rocka i
                        innych gatunków muzycznych. Zespół cechuje nietypowy, wirtualny image. W rzeczywistości w zespole od
                        strony muzycznej na stałe jest tylko
                        Damon Albarn, który współpracuje z bardzo wieloma różnymi muzykami na potrzeby danego utworu. Przez
                        wiele pierwszych lat członkowie nie
                        pokazywali się osobiście, nawet na koncertach (z wyjątkiem pięciu występów w Manchesterze i Nowym
                        Jorku) czy rozdaniach nagród, a zastępowali
                        to wirtualnymi postaciami zespołu Gorillaz (2-D, Noodle, Murdoc oraz Russel). Obecnie Albarn
                        otwarcie występuje osobiście na koncertach wraz z
                        całym gronem współpracujących muzyków w składzie zależnym od granego utworu.</p>
                </MDBContainer>
            </Container>
            <Container maxWidth={"sm"} style={{marginTop:'30px', marginBottom:'100px'}}>
                <YoutubeFilm embedId="5qJp6xlKEug" />
            </Container>
        </div>
    );
};
export default Info;