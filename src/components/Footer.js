import React from "react";
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InfoIcon from '@mui/icons-material/Info';
import YouTubeIcon from '@mui/icons-material/YouTube';
import {
    Box,
    Container,
    Row,
    Column,
    FooterLink,
    Heading,
} from "./FooterStyles";

const Footer = () => {
    return (
        <footer className="footer">
            <Box>
                <h2 style={{
                    color: "green",
                    textAlign: "center",
                    marginTop: "-50px"
                }}>
                    Gorillaz-fans.pl: Strona poświęcona zespołowi Gorillaz
                </h2>
                <Container>
                    <Row>
                        <Column>
                            <Heading>O nas</Heading>
                            <FooterLink href="/info"><InfoIcon/> Informacje</FooterLink>
                        </Column>
                        <Column>
                            <Heading>Social Media</Heading>
                            <FooterLink href="https://www.facebook.com/Gorillaz">
                                <i className="fab fa-facebook-f">
                <span style={{marginLeft: "10px"}}>
                    <FacebookIcon/> Facebook
                </span>
                                </i>
                            </FooterLink>
                            <FooterLink href="https://www.instagram.com/gorillaz/">
                                <i className="fab fa-instagram">
                <span style={{marginLeft: "10px"}}>
                  <InstagramIcon/> Instagram
                </span>
                                </i>
                            </FooterLink>
                            <FooterLink
                                href="https://twitter.com/gorillaz?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">
                                <i className="fab fa-twitter">
                <span style={{marginLeft: "10px"}}>
                  <TwitterIcon/> Twitter
                </span>
                                </i>
                            </FooterLink>
                            <FooterLink href="https://www.youtube.com/channel/UCfIXdjDQH9Fau7y99_Orpjw">
                                <i className="fab fa-youtube">
                <span style={{marginLeft: "10px"}}>
                  <YouTubeIcon/> Youtube
                </span>
                                </i>
                            </FooterLink>
                        </Column>
                    </Row>
                </Container>
            </Box>
        </footer>
    );
};
export default Footer;