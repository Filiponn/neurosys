import {AppBar, Container, MenuItem, Toolbar, Typography} from "@mui/material";
import {Link, Route, Routes} from "react-router-dom";
import MainPage from "./MainPage";
import Info from "./Info";
import InfoIcon from '@mui/icons-material/Info';
const Navbar = () => {
    // https://medium.com/frontendweb/how-to-pass-state-or-data-in-react-router-v6-c366db9ee2f4

    return (
        <div>
            <AppBar position="fixed" style={{ background: 'black' }}>

                <Container maxWidth="xl">
                    <Toolbar>
                        <Link to="/">
                            <MenuItem>
                                <img src="https://st.depositphotos.com/1000423/2114/i/450/depositphotos_21148539-stock-photo-audio-microphone-retro-style.jpg"
                                     alt="logo" width="100px" height="auto"/>
                                <Typography variant="h6" style={{ color: 'white', paddingLeft:"10px"}}>
                                    Strona główna
                                </Typography>
                            </MenuItem>
                        </Link>
                        <Link to="/info">
                            <MenuItem>
                               <InfoIcon color={"primary"}></InfoIcon>
                                <Typography variant="h6" style={{ color: 'white', paddingLeft:"10px"}}>
                                    O nas
                                </Typography>
                            </MenuItem>
                        </Link>

                    </Toolbar>
                </Container>
            </AppBar>

            <Routes>
                <Route path="/" element={<MainPage/>}>
                </Route>
                <Route path="/info" element={<Info/>}>
                </Route>

            </Routes>

        </div>

    );


}

export default Navbar;