import React from 'react';
import {useFormik} from 'formik';
import * as yup from "yup";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import "yup-phone";
import {useState} from "react";
import {Box, Card, Container} from "@mui/material";
import SendIcon from '@mui/icons-material/Send';


const validationSchema = yup.object({
    name: yup
        .string('Wpisz imię i nazwisko')
        .required('Imię jest wymagane'),
    email: yup
        .string('Wpisz email')
        .email('Niepoprawny email')
        .required('Email jest wymagany'),
    phone: yup
        .string('Wpisz numer telefonu (z kodem kraju)')
        .phone("IN", false, 'Niepoprawny numer telefonu')
        .required('Numer telefonu jest wymagany'),
    msg: yup
        .string('Wpisz wiadomość')
        .required('Treść wiadomości jest wymagana'),
});

function FormikUI(props) {

    const questions = [
        {
            questionText: 'Kiedy powstał zespół Gorillaz?',
            answerOptions: [
                {answerText: '1989', isCorrect: false},
                {answerText: '2000', isCorrect: false},
                {answerText: '1987', isCorrect: false},
                {answerText: '1998', isCorrect: true},
            ],
        },
        {
            questionText: 'Skąd pochodzi zespół?',
            answerOptions: [
                {answerText: 'USA', isCorrect: false},
                {answerText: 'Anglia', isCorrect: true},
                {answerText: 'Francja', isCorrect: false},
                {answerText: 'Niemcy', isCorrect: false},
            ],
        },
        {
            questionText: 'Który album powstał w 2010 roku?',
            answerOptions: [
                {answerText: 'Plastic Beach', isCorrect: true},
                {answerText: 'Intel', isCorrect: false},
                {answerText: 'Hot Beach', isCorrect: false},
                {answerText: 'Dungeon', isCorrect: false},
            ],
        },
        {
            questionText: 'Kiedy zespół po raz pierwszy wystąpił w Polsce?',
            answerOptions: [
                {answerText: '2018', isCorrect: false},
                {answerText: '2000', isCorrect: false},
                {answerText: '2011', isCorrect: false},
                {answerText: '2017', isCorrect: true},
            ],
        },
        {
            questionText: 'Kiedy zespół wystąpił na Openerze?',
            answerOptions: [
                {answerText: '2018', isCorrect: false},
                {answerText: '2016', isCorrect: false},
                {answerText: '2011', isCorrect: false},
                {answerText: '2018', isCorrect: true},
            ],
        },
        {
            questionText: 'Jak nazywa się główny wokalista zespołu oraz założyciel?',
            answerOptions: [
                {answerText: 'Damon Albarn', isCorrect: true},
                {answerText: 'Jamie Hewlett', isCorrect: false},
                {answerText: 'Josh Mac', isCorrect: false},
                {answerText: 'Jamie Sturn', isCorrect: false},
            ],
        },
    ];


    const [currentQuestion, setCurrentQuestion] = useState(Math.floor(Math.random() * questions.length));
    const [disable, setDisable] = useState(true);
    const [disableAns, setDisableAns] = useState(false);
    const [error, setError] = useState(false);

    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setDisable(false);
            setDisableAns(true);
            setError(false)
        } else {
            const nextQuestion = Math.floor(Math.random() * questions.length);
            setCurrentQuestion(nextQuestion);
            setError(true)
        }
    }

    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            phone: '',
            msg: '',
        },
        validationSchema: validationSchema,
        onSubmit: (event) => {
            props.addForm(event);
        },
    });


    return (
        <div>

            <div className={"modal_row"} style={{textAlign: "center", paddingTop: 100}}>
                <Container maxWidth={"sm"}>
                    <Card style={{marginTop: '100px'}}>
                        <Container maxWidth={"sm"}>
                            <h1>Wiadomość do menedżera!</h1>
                            <form onSubmit={formik.handleSubmit}>
                                <div style={{marginTop: 10}}>
                                    <TextField
                                        fullWidth
                                        id="name"
                                        name="name"
                                        label="Imię i nazwisko"
                                        value={formik.values.name}
                                        onChange={formik.handleChange}
                                        error={formik.touched.name && Boolean(formik.errors.name)}
                                        helperText={formik.touched.name && formik.errors.name}
                                        placeholder="Proszę wpisać imię i nazwisko"
                                    />
                                </div>
                                <div style={{marginTop: 10}}>
                                    <TextField
                                        fullWidth
                                        id="email"
                                        name="email"
                                        label="E-mail"
                                        type="email"
                                        value={formik.values.email}
                                        onChange={formik.handleChange}
                                        error={formik.touched.email && Boolean(formik.errors.email)}
                                        helperText={formik.touched.email && formik.errors.email}
                                        placeholder="Proszę wpisać e-mail"
                                    />
                                    <div style={{marginTop: 10}}>
                                        <TextField
                                            fullWidth
                                            id="phone"
                                            name="phone"
                                            type="tel"
                                            label="Numer telefonu"
                                            value={formik.values.phone}
                                            onChange={formik.handleChange}
                                            error={formik.touched.phone && Boolean(formik.errors.phone)}
                                            helperText={formik.touched.phone && formik.errors.phone}
                                            placeholder="Proszę wpisać numer telefonu"
                                        />

                                    </div>
                                    <div style={{marginTop: 10}}>
                                        <TextField
                                            fullWidth
                                            id="msg"
                                            name="msg"
                                            label="Treść wiadomości"
                                            value={formik.values.msg}
                                            onChange={formik.handleChange}
                                            error={formik.touched.msg && Boolean(formik.errors.msg)}
                                            helperText={formik.touched.msg && formik.errors.msg}
                                            placeholder="Proszę wpisać treść wiadomości"
                                        />
                                    </div>
                                    <Card sx={{backgroundColor: '#eeeeee'}} style={{marginTop: '20px'}}>
                                        <div className='question-section'>
                                            <Box bgcolor={'dodgerblue'} style={{padding:'1px'}}>
                                                <h3 style={{color:'white'}}>
                                                    Odpowiedź na pytanie!
                                                </h3>
                                            </Box>

                                            {error ? <p style={{color: "red"}}>Źle! Spróbuj ponownie!</p> : null}
                                            <h4>
                                                <div
                                                    className='question-text'>{questions[currentQuestion].questionText}</div>
                                            </h4>
                                        </div>
                                        <div className='answer-section'>
                                            {questions[currentQuestion].answerOptions.map((answerOption) => (
                                                <button style={{padding: '5px', margin: '5px'}}
                                                        onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}
                                                        disabled={disableAns}>{answerOption.answerText}</button>
                                            ))}
                                        </div>
                                    </Card>

                                </div>

                                <div style={{marginTop: 20, marginBottom: 20}}>
                                    <Button size="large" color="primary" variant="contained" type="submit"
                                            disabled={disable}
                                            endIcon={<SendIcon/>}>
                                        Wyślij
                                    </Button>
                                </div>
                            </form>
                        </Container>
                    </Card>
                </Container>
            </div>

        </div>
    );

}

export default FormikUI;

